//
//  AppDelegate.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 1/29/21.
//

import UIKit
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		FirebaseApp.configure()
		signinUser()
		customizeNavBar()
		return true
	}

	// MARK: UISceneSession Lifecycle

	func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
		// Called when a new scene session is being created.
		// Use this method to select a configuration to create the new scene with.
		return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
	}

	func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
		// Called when the user discards a scene session.
		// If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
		// Use this method to release any resources that were specific to the discarded scenes, as they will not return.
	}
	
	private func signinUser() {
		Auth.auth().signIn(withEmail: "yamadou21@gmail.com", password: "P@sser123") { [weak self] authResult, error in
			guard let _ = self else { return }
			guard error == nil else {
				print("Error signing in user: \(error?.localizedDescription)")
				return
			}
			print("Successfully logged in: \(authResult?.user)")
		}
	}
	
	private func customizeNavBar() {
		let attrs = [
			NSAttributedString.Key.foregroundColor: UIColor.black,
			NSAttributedString.Key.font: Theme.Fonts.proximaNovaBold(size: 18).value
		]
		
		UINavigationBar.appearance().titleTextAttributes = attrs
	}
}

