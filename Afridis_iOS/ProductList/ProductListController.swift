//
//  ViewController.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 1/29/21.
//

import UIKit

class ProductListController: UIViewController {
	
	let viewModel: ProductListViewModel
	
	private lazy var productListView: ProductListView = {
		let view = ProductListView(frame: UIScreen.main.bounds)
		return view 
	}()
		
	
	lazy var collectionView: UICollectionView = {
		let collectionView = productListView.collectionView
		collectionView.delegate = self
		collectionView.dataSource = self
		return collectionView
	}()
	
	init(viewModel: ProductListViewModel) {
		self.viewModel = viewModel
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		title = "L'Africaine de La Distribution"
        navigationController?.navigationBar.tintColor = .black
		collectionView.register(ProductListCell.self, forCellWithReuseIdentifier: ProductListCell.cellID)
		fetchProducts()
	}
	
	override func loadView() { self.view = productListView }
	
    private func fetchProducts() {
        viewModel.fetchProducts {
            self.collectionView.reloadData()
        }
    }
}

// MARK: - UICollectionViewDelegate
extension ProductListController: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let productDetailVC = ProductDetailController(viewModel: ProductDetailViewModel(product: viewModel.products[indexPath.row]))
		navigationController?.pushViewController(productDetailVC, animated: true)
	}
}

// MARK: - UICollectionViewDataSource
extension ProductListController: UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return viewModel.products.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductListCell.cellID, for: indexPath) as? ProductListCell else {
			return UICollectionViewCell()
		}
		cell.product = viewModel.products[indexPath.row]
		return cell
	}
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ProductListController: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let width = (view.frame.width - 36) / 2
		let height = (width * 5) / 3.5
		return .init(width: width, height: height)
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		return .init(top: 24, left: 12, bottom: 12, right: 12)
	}
}


