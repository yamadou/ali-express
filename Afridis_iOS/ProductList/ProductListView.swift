//
//  ProductListView.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 1/29/21.
//

import UIKit

class ProductListView: UIView {

	let collectionView: UICollectionView = {
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
		collectionView.backgroundColor = .clear
		collectionView.alwaysBounceVertical = true
		return collectionView
	}()
	
    let callBtn: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Appelez pour commander", for: .normal)
        btn.titleLabel?.font = Theme.Fonts.proximaNovaBold(size: 16).value
        btn.setTitleColor(#colorLiteral(red: 0.9960784314, green: 0.9450980392, blue: 0.9450980392, alpha: 1), for: .normal)
        btn.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.2745098039, blue: 0.2745098039, alpha: 1)
        btn.addTarget(self, action: #selector(makePhoneCall), for: .touchUpInside)
        return btn
    }()
    
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupViews()
	}
	
	private func setupViews() {
		backgroundColor = .init(white: 0.95, alpha: 1)
        add(subviews: [collectionView, callBtn])
        collectionView.top(to: self)
            .leading(to: self)
            .trailing(to: self)
        
        callBtn.top(with: collectionView.bottomAnchor)
            .leading(to: self)
            .trailing(to: self)
            .bottom(with: layoutMarginsGuide.bottomAnchor)
            .height(value: 49)
	}
    
    @objc func makePhoneCall() {
        guard let numberUrl = URL(string: "tel://778292807") else { return }
        UIApplication.shared.open(numberUrl)
    }
    
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
