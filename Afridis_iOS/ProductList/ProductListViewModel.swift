//
//  ProductListViewModel.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 1/31/21.
//

import Foundation

class ProductListViewModel {
	
	var products: [Product] = []
	
	func fetchProducts(completion: @escaping () -> ()) {
		Service.shared.fetchProducts { (products, err) in
			guard err == nil else { return }
			self.products = products ?? []
            for p in self.products {
                print(p.name)
            }
			completion()
		}
	}
}
