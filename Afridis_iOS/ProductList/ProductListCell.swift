//
//  ProductListCellCollectionViewCell.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 1/30/21.
//

import UIKit
import Firebase

class ProductListCell: UICollectionViewCell {
	
	static let cellID = "ProductListCellID"
	
	var product: Product? {
		didSet {
			productNameLabel.text = product?.name
			productPriceLabel.text = "\(product?.price ?? 0) CFA"
			MOQLabel.text = "le lot de \(product?.MOQ ?? 0) unités"
            Service.shared.getMediaData(forUrl: product?.imagesUrl.first) { data in
                guard let data = data else { return }
                self.productImageView.image = UIImage(data: data)
            }
		}
	}
	
	let productImageView: UIImageView = {
		let imgView = UIImageView()
		return imgView
	}()
	
	let productNameLabel: UILabel = {
		let label = UILabel()
		label.font = Theme.Fonts.proximaNovaRegular(size: 15).value
		return label
	}()
	
	let productPriceLabel: UILabel = {
		let label = UILabel()
		label.font = Theme.Fonts.proximaNovaBold(size: 18).value
		return label
	}()
	
	let MOQLabel: UILabel = {
		let label = UILabel()
		label.font = Theme.Fonts.proximaNovaRegular(size: 14).value
		label.textColor = #colorLiteral(red: 0.6008173823, green: 0.5959553719, blue: 0.595927, alpha: 1)
		return label
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupViews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupViews() {
		backgroundColor = .white
		clipsToBounds = true
		cornerRadius = 10
		
		add(subviews: [productImageView, productNameLabel, productPriceLabel, MOQLabel])

		productImageView.top(to: self)
			.leading(to: self)
			.trailing(to: self)
			.height(value: (frame.height * 2) / 3)
		
		productNameLabel.top(with: productImageView.bottomAnchor, value: 8)
			.leading(to: self, value: 8)
			.trailing(to: self, value: -8)
		
		productPriceLabel.top(with: productNameLabel.bottomAnchor, value: 8)
			.leading(to: self, value: 8)
			.trailing(to: self, value: -8)
		
		MOQLabel.top(with: productPriceLabel.bottomAnchor, value: 2)
			.leading(to: self, value: 8)
			.trailing(to: self, value: -8)
			.bottom(to: self, value: -4)
	}
}
