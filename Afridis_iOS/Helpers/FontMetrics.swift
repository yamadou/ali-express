//
//  FontMetrics.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 1/30/21.
//

import UIKit

/// A `UIFontMetrics` wrapper, allowing iOS 11 devices to take advantage of `UIFontMetrics` scaling,
/// while earlier iOS versions fall back on a scale calculation.
///
struct FontMetrics {
	
	/// A scale value based on the current device text size setting. With the device using the
	/// default Large setting, `scaler` will be `1.0`. Only used when `UIFontMetrics` is not
	/// available.
	///
	static var scaler: CGFloat {
		return UIFont.preferredFont(forTextStyle: .body).pointSize / 17.0
	}
	
	/// Returns a version of the specified font that adopts the current font metrics.
	///
	/// - Parameter font: A font at its default point size.
	/// - Returns: The font at its scaled point size.
	///
	static func scaledFont(for font: UIFont, size: CGFloat) -> UIFont {
		if #available(iOS 11.0, *) {
			switch size {
			case 10...11: return UIFontMetrics(forTextStyle: .caption2).scaledFont(for: font)
			case 12: return UIFontMetrics(forTextStyle: .caption1).scaledFont(for: font)
			case 13: return UIFontMetrics(forTextStyle: .footnote).scaledFont(for: font)
			case 14: return UIFontMetrics(forTextStyle: .callout).scaledFont(for: font)
			case 15...17: return UIFontMetrics(forTextStyle: .body).scaledFont(for: font)
			case 18...20: return UIFontMetrics(forTextStyle: .title3).scaledFont(for: font)
			case 21...22: return UIFontMetrics(forTextStyle: .title2).scaledFont(for: font)
			case 23...28: return UIFontMetrics(forTextStyle: .title1).scaledFont(for: font)
			case 28...34: return UIFontMetrics(forTextStyle: .largeTitle).scaledFont(for: font)
			default: return UIFontMetrics.default.scaledFont(for: font)
			}
		} else {
			return font.withSize(scaler * font.pointSize)
		}
	}
	
}
