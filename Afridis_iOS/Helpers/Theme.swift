//
//  Theme.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 1/30/21.
//

import UIKit


public enum Theme {
	
	public enum Fonts {
	  
	  case proximaNovaRegular(size: CGFloat)
	  case proximaNovaThin(size: CGFloat)
	  case proximaNovaBold(size: CGFloat)
	  case proximaNovaExtraBold(size: CGFloat)
	  case proximaNovaBlack(size: CGFloat)
	  
	  public var value: UIFont {
		switch self {
		  case .proximaNovaRegular(let size):
			let font = UIFont(name: "ProximaNova-Regular", size: size)!
			return FontMetrics.scaledFont(for: font, size: size)
		  case .proximaNovaThin(let size):
			let font = UIFont(name: "ProximaNovaT-Thin", size: size)!
			return FontMetrics.scaledFont(for: font, size: size)
		  case .proximaNovaBold(let size):
			let font = UIFont(name: "ProximaNova-Bold", size: size)!
			return FontMetrics.scaledFont(for: font, size: size)
		  case .proximaNovaExtraBold(let size):
			let font = UIFont(name: "ProximaNova-Extrabld", size: size)!
			return FontMetrics.scaledFont(for: font, size: size)
		case .proximaNovaBlack(let size):
		  let font = UIFont(name: "ProximaNova-Black", size: size)!
		  return FontMetrics.scaledFont(for: font, size: size)
		}
	  }
	}
}
