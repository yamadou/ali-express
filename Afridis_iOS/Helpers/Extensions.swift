//
//  Extensions.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 1/30/21.
//

import UIKit
import MBProgressHUD

extension UILabel {
	convenience init(text: String, font: UIFont, numberOfLines: Int = 1) {
		self.init(frame: .zero)
		self.text = text
		self.font = font
		self.numberOfLines = numberOfLines
	}
}

extension UIViewController {
    func showHUD(title: String, description:String) {
        let indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        indicator.label.text = title
        indicator.isUserInteractionEnabled = false
        indicator.detailsLabel.text = description
        indicator.show(animated: true)
    }
    func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
