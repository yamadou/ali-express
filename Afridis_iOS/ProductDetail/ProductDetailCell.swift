//
//  ProductDetailCell.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 2/1/21.
//

import UIKit

class ProductDetailCell: UICollectionViewCell {
	
    var wholesalePriceElementView = PricingElementView(title: "000 CFA", value: "le lot de 12 pièces")
	var minimumOrderQtyElementView = PricingElementView(title: "000 pièces", value: "commande minimale")
	var unitPriceElementView = PricingElementView(title: "000 CFA", value: "prix unitaire")
	var suggestedRetailPriceElementView = PricingElementView(title: "000 CFA", value: "prix de vente conseillé")

    let c: UIColor = #colorLiteral(red: 0.3999959826, green: 0.4067792296, blue: 0.4932877421, alpha: 1)
	
    var product: Product? {
        didSet {
            guard let product = product else { return }
            unitPriceElementView.titleLabel.text = "\(product.price)"
            minimumOrderQtyElementView.titleLabel.text = "\(product.MOQ)"
            wholesalePriceElementView.titleLabel.text = "\(product.MOQ * product.price)"
            suggestedRetailPriceElementView.titleLabel.text = "\(product.recommendedRetailPrice)"
        }
    }
    
	let descriptionLabel: UILabel = {
		let label = UILabel()
		label.font = Theme.Fonts.proximaNovaRegular(size: 15).value
		label.text = "Creative Straw Cup Sequined Glitter Cup Colorful Coffee Juice Straw Mug simple Cute Net Red Plastic Botto Outdoor Portable Cup Creative Straw Cup Sequined Glitter Cup"
		label.numberOfLines = 3
		return label
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupViews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupViews() {
		backgroundColor = .white
		
		let pricingFirstColumnStackView = UIStackView(arrangedSubviews: [unitPriceElementView, minimumOrderQtyElementView])
		pricingFirstColumnStackView.axis = .vertical
		pricingFirstColumnStackView.spacing = 16
		
		let pricingSecondColumnStackView = UIStackView(arrangedSubviews: [suggestedRetailPriceElementView, wholesalePriceElementView])
		pricingSecondColumnStackView.axis = .vertical
		pricingSecondColumnStackView.spacing = 16

		let containerStackView = UIStackView(arrangedSubviews: [pricingFirstColumnStackView, pricingSecondColumnStackView])
		containerStackView.spacing = 16
		
		add(subviews: [containerStackView, descriptionLabel])
		
		descriptionLabel.top(to: self, value: 16)
			.leading(to: self, value: 16)
			.trailing(to: self, value: -16)
		
		containerStackView.top(with: descriptionLabel.bottomAnchor, value: 24)
			.centerX(to: self)
	}
}

class PricingElementView: UIView {
	
	let titleLabel: UILabel = {
		let label = UILabel()//
		label.font = Theme.Fonts.proximaNovaBold(size: 18).value
		label.textAlignment = .center
		return label
	}()
	
	let valueLabel: UILabel = {
		let label = UILabel()
		label.font = Theme.Fonts.proximaNovaRegular(size: 13).value
		label.textColor = #colorLiteral(red: 0.6008173823, green: 0.5959553719, blue: 0.595927, alpha: 1)
		label.textAlignment = .center
		return label
	}()
	
	init(title: String, value: String) {
		titleLabel.text = title
		valueLabel.text = value
		super.init(frame: UIScreen.main.bounds)
		setupViews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupViews() {
		backgroundColor = .white
		add(subviews: [titleLabel, valueLabel])
		titleLabel.top(to: self)
			.leading(to: self, value: 16)
			.trailing(to: self, value: -16)
		
		valueLabel.top(with: titleLabel.bottomAnchor, value: 4)
			.leading(to: titleLabel)
			.trailing(to: titleLabel)
			.bottom(to: self)
	}
}
