//
//  ProductMediaHorizontalController.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 2/1/21.
//

import UIKit
import AVKit

enum MediaCell {
    case image
    case video
    
    init(row: Int) {
        switch row {
        case 0:
            self = .video
        default:
            self = .image
        }
    }
}

class HeaderMediaController: HorizontalSnappingController {
	
	let imageCellId = "imageCellId"
    let videoCellId = "videoCellId"
    
	var product: Product? {
		didSet {
			collectionView.reloadData()
		}
	}
    
    lazy var headerMediaView: HeaderMediaView = {
        return view.superview as! HeaderMediaView
    }()
				
	override func viewDidLoad() {
		super.viewDidLoad()
		setupCollectionView()
	}
	
	private func setupCollectionView() {
		collectionView.backgroundColor = .init(white: 0.95, alpha: 1)
		collectionView.register(HeaderImageCell.self, forCellWithReuseIdentifier: imageCellId)
        collectionView.register(HeaderVideoCell.self, forCellWithReuseIdentifier: videoCellId)
		if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
			layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
		}
		collectionView.showsHorizontalScrollIndicator = false
	}
	
	override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return product?.imagesUrl.count ?? 0
	}
	
	override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellType = MediaCell(row: indexPath.row)
        switch cellType {
        case .video:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: videoCellId, for: indexPath) as! HeaderVideoCell
            cell.videoUrlString = product?.videosUrl.first ?? ""
            cell.thumbnailImageUrl = product?.imagesUrl.first
            return cell
            
        case .image:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imageCellId, for: indexPath) as! HeaderImageCell
            cell.imageUrl = product?.imagesUrl[indexPath.row]
            return cell
        }
	}
    
    override func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if MediaCell(row: indexPath.row) == .video {
            (cell as? HeaderVideoCell)?.playerViewController.player?.pause()
        }
    }
}

// MARK: UICollectionViewDelegateFlowLayout
extension HeaderMediaController: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return .init(width: view.frame.width, height: view.frame.height)
	}
}

