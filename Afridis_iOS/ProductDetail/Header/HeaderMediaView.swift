//
//  ProductMediaView.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 2/1/21.
//

import UIKit

class HeaderMediaView: UICollectionReusableView {
	
	let headerMediaController = HeaderMediaController()
    
    var counterLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = Theme.Fonts.proximaNovaBold(size: 13).value
        return label
    }()
	
    var pictureImageView: UIImageView = {
        let imgView = UIImageView()
        imgView.image = UIImage(named: "picture")?.withRenderingMode(.alwaysTemplate)
        imgView.height(value: 15).width(value: 15)
        imgView.contentMode = .scaleAspectFit
        imgView.tintColor = UIColor.white
        return imgView
    }()
    
    lazy var counterView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.4159607291, green: 0.4599673748, blue: 0.4765977263, alpha: 1).withAlphaComponent(0.5)
        view.width(value: 50).height(value: 30)
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 15
        view.add(subviews: [counterLabel, pictureImageView])
        pictureImageView.leading(to: view, value: 8).centerY(to: view)
        counterLabel.trailing(to: view, value: -8).centerY(to: view)
        return view
    }()
    
	override init(frame: CGRect) {
		super.init(frame: frame)
		
        add(subviews: [headerMediaController.view, counterView])
		headerMediaController.view.snap(to: self)
        counterView.trailing(to: headerMediaController.view, value: -16)
            .top(to: headerMediaController.view, value: 16)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

