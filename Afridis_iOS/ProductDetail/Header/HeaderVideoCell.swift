//
//  VideoViewCell.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 2/13/21.
//

import UIKit
import AVKit

class HeaderVideoCell: UICollectionViewCell {
    
    let playerViewController = AVPlayerViewController()
    
    var videoUrlString: String?
    var thumbnailImageView: UIImageView?
    
    var thumbnailImageUrl: String? {
        didSet {
            guard oldValue != thumbnailImageUrl else { return }
            Service.shared.getMediaData(forUrl: thumbnailImageUrl) { data in
                guard let data = data else { return }
                self.thumbnailImageView = UIImageView(image: UIImage(data: data))
                
                DispatchQueue.main.async {
                    self.playerViewController.contentOverlayView?.addSubview(self.thumbnailImageView ?? UIView())
                    self.thumbnailImageView?.snap(to: self.playerViewController.contentOverlayView!)
                    self.addTapGestureRecognizer()
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(playerViewController.view)
        playerViewController.view.snap(to: self)
    }
    
    private func addTapGestureRecognizer() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(playVideo))
        self.addGestureRecognizer(tapGesture)
    }
    
    private func removeTapGesture() {
        guard let gestures = gestureRecognizers else { return }
        for gesture in gestures {
            if gesture is UITapGestureRecognizer {
                removeGestureRecognizer(gesture)
            }
        }
    }
    
    @objc private func playVideo() {
        thumbnailImageView?.removeFromSuperview()
        removeTapGesture()
        Service.shared.getDownloadUrl(for: videoUrlString) { (url, err) in
            if let err = err {
                print("Error retrieving donwloadUrl: \(err.localizedDescription)")
                return
            }
            guard let videoUrl = url else { return }
            
            let data = Service.shared.getVideoDataFromMemory(videoUrl: self.videoUrlString ?? "")
            guard let videoData = data else { self.fetchRemoteVideo(url: videoUrl); return }
            self.fetchVideoFromMemory(data: videoData)
        }
    }
    
    private func fetchVideoFromMemory(data: Data) {
        let playerItem = CachingPlayerItem(data: data, mimeType: "video/mp4", fileExtension: "mp4")
        setupAVPlayer(with: playerItem)
    }
    
    private func fetchRemoteVideo(url: URL) {
        let playerItem = CachingPlayerItem(url: url)
        playerItem.delegate = self
        setupAVPlayer(with: playerItem)
    }
    
    private func setupAVPlayer(with playerItem: AVPlayerItem) {
        let player = AVPlayer(playerItem: playerItem)
        player.automaticallyWaitsToMinimizeStalling = false
        self.playerViewController.player = player
        player.play()
    }
}

extension HeaderVideoCell: CachingPlayerItemDelegate {
    func playerItem(_ playerItem: CachingPlayerItem, didFinishDownloadingData data: Data) {
        guard let videoUrlString = videoUrlString else { return }
        Service.shared.saveVideoDataToMemory(videoUrl: videoUrlString, data: data)
    }
}
