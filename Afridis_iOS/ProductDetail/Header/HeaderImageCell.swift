//
//  ProductMediaHeaderCell.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 2/1/21.
//

import UIKit

class HeaderImageCell: UICollectionViewCell {
    
    var imageUrl: String? {
        didSet {
            Service.shared.getMediaData(forUrl: imageUrl) { data in
                guard let data = data else { return }
                self.imageView.image = UIImage(data: data)
            }
        }
    }
	
	let imageView: UIImageView = {
		let imgView = UIImageView()
		imgView.backgroundColor = .init(white: 0.75, alpha: 1)
		return imgView
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupViews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupViews() {
		add(subviews: [imageView])
		imageView.snap(to: self)
	}
}
