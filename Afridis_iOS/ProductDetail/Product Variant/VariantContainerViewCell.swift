//
//  ProductVariantView.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 2/7/21.
//
import UIKit

class VariantContainerViewCell: UICollectionViewCell {
	
	let productVariantController = VariantController()
	
	let titleLabel: UILabel = {
		let label = UILabel()
		label.font = Theme.Fonts.proximaNovaBold(size: 18).value
		label.text = "Couleurs disponibles "
		return label
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		backgroundColor = .white
		addSubview(productVariantController.view)
		addSubview(titleLabel)
		titleLabel.top(to: self, value: 8)
			.leading(to: self, value: 16)
			.trailing(to: self, value: -16)
		
		productVariantController.view.top(with: titleLabel.bottomAnchor, value: 16)
			.leading(to: self)
			.trailing(to: self)
			.height(value: 75)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

