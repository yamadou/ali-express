//
//  ProductVariantCollectionViewController.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 2/7/21.
//

import UIKit

private let reuseIdentifier = "productVariantCell"

class VariantController: HorizontalSnappingController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Register cell classes
		setupCollectionView()
	}
	
	private func setupCollectionView() {
		collectionView.backgroundColor = .white
		self.collectionView!.register(VariantCell.self, forCellWithReuseIdentifier: reuseIdentifier)
		if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
			layout.scrollDirection = .horizontal
		}
		collectionView.alwaysBounceHorizontal = true
		collectionView.showsHorizontalScrollIndicator = false
	}
	
	override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 10
	}
	
	override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! VariantCell
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return 16
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		return .init(top: 0, left: 16, bottom: 0, right: 16)
	}
	
}

// MARK: - UICollectionViewDelegateFlowLayout
extension VariantController: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return .init(width: 75, height: 75)
	}
}
