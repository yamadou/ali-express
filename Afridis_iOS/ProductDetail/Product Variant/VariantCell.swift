//
//  ProductVariantCellCollectionViewCell.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 2/7/21.
//

import UIKit

class VariantCell: UICollectionViewCell {
	
	let imageView: UIImageView = {
		let imgView = UIImageView()
		imgView.backgroundColor = .init(white: 0.95, alpha: 1)
		imgView.layer.cornerRadius = 10
		return imgView
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupViews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupViews() {
		addSubview(imageView)
		imageView.snap(to: self)
	}
}
