//
//  ProductDetailViewController.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 2/1/21.
//

import UIKit

class ProductDetailController: UIViewController {
	
	let viewModel: ProductDetailViewModel
	let headerId = "headerId"
	let productDetailCellID = "productDetailCellId"
	let productVariantCellID = "productVariantCellID"
	
	private lazy var productDetailView: ProductDetailView = {
		let view = ProductDetailView(frame: UIScreen.main.bounds)
        view.delegate = self
		return view
	}()
	
	lazy var collectionView: UICollectionView = {
		let collectionView = productDetailView.collectionView
		collectionView.dataSource = self
		collectionView.delegate = self
		return collectionView
	}()
	
	init(viewModel: ProductDetailViewModel) {
		self.viewModel = viewModel
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func loadView() { self.view = productDetailView }
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Do any additional setup after loading the view.
        title = viewModel.product.name
		setupCollectionView()
    }
	
	private func setupCollectionView() {
        collectionView.register(HeaderMediaView.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: headerId)
        collectionView.register(ProductDetailCell.self, forCellWithReuseIdentifier: productDetailCellID)
        collectionView.register(VariantContainerViewCell.self, forCellWithReuseIdentifier: productVariantCellID)
	}
    
    override func viewWillDisappear(_ animated: Bool) {
        toggleVideo(shouldPlay: false)
    }
}

// MARK: - UICollectionViewDataSource
extension ProductDetailController: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! HeaderMediaView
        header.counterLabel.text = "\(viewModel.product.imagesUrl.count)"
        header.headerMediaController.product = viewModel.product
		return header
	}
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 2
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let section = viewModel.getSectionType(indexPath.section)
		switch section {
		case .description:
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: productDetailCellID, for: indexPath) as! ProductDetailCell
            cell.product = viewModel.product
            return cell
		case .variant:
			return collectionView.dequeueReusableCell(withReuseIdentifier: productVariantCellID, for: indexPath) as! VariantContainerViewCell
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		let sectionType = viewModel.getSectionType(section)
		return sectionType == .description
			? .zero
			: .init(top: 8, left: 0, bottom: 0, right: 0)
	}
    
    // MARK: - Private func
    func toggleVideo(shouldPlay: Bool) {
        let header = collectionView.visibleSupplementaryViews(ofKind: UICollectionView.elementKindSectionHeader).first as? HeaderMediaView
        let videoCell = header?.headerMediaController.collectionView.cellForItem(at: IndexPath(row: 0, section: 0)) as?  HeaderVideoCell
        videoCell?.playerViewController.player?.rate = shouldPlay ? 1 : 0
    }
}

// MARK: - UICollectionViewDelegate
extension ProductDetailController: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
		let sectionType = viewModel.getSectionType(section)
		return sectionType == .description
			? .init(width: view.frame.width, height: (view.frame.height / 2) - 30)
			: .init(width: 0, height: 0)
	}
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ProductDetailController: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let sectionType = viewModel.getSectionType(indexPath.section)
		return sectionType == .description
			?	.init(width: view.frame.width, height: 250)
			: .init(width: view.frame.width, height: 140)
	}
}

// MARK: - ProductDetailViewDelegate
extension ProductDetailController: ProductDetailViewDelegate {
    func callToPlaceOrderDidTap(_ button: UIButton) {
        viewModel.makePhoneCall()
    }
    
    func downloadMediaDidTap(_ button: UIButton) {
        let downloadImagesAction = UIAlertAction(title: "Sauvegarder Images", style: .default) { action in
            self.toggleVideo(shouldPlay: false)
            self.saveImagesToSavedPhotosAlbum()
        }
        
        let downloadVideoAction = UIAlertAction(title: "Sauvegarder Vidéo", style: .default) { action in
            self.toggleVideo(shouldPlay: false)
            self.saveVideoToSavedPhotosAlbum()
        }
        
        let cancelAction = UIAlertAction(title: "Annuler", style: .cancel, handler: nil)
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(downloadVideoAction)
        alertController.addAction(downloadImagesAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    private func saveImagesToSavedPhotosAlbum() {
        showHUD(title: "Un instant", description: "Sauvegarde en cours . . .")
        viewModel.saveImagesToSavedPhotosAlbum {
            DispatchQueue.main.async {
                self.hideHUD()
                self.showMediaSavedSuccessAlert()
            }
        }
    }
    
    private func saveVideoToSavedPhotosAlbum() {
        showHUD(title: "Un instant", description: "Sauvegarde en cours . . .")
        guard let videoUrl = viewModel.product.videosUrl.first else { return }
        viewModel.saveVideoToSavedPhotosAlbum(urlString: videoUrl ?? "") {
            DispatchQueue.main.async {
                self.hideHUD()
                self.showMediaSavedSuccessAlert()
            }
        }  
    }
    
    private func showMediaSavedSuccessAlert() {
        let alertController = UIAlertController(title: "Sauvegarde Terminée", message: nil, preferredStyle: .alert)
        self.present(alertController, animated: true, completion: nil)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            alertController.dismiss(animated: true) {
                self.toggleVideo(shouldPlay: true)
            }
        }
    }
}
