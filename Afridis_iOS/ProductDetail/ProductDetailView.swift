//
//  ProductDetailView.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 2/1/21.
//

import UIKit

protocol ProductDetailViewDelegate {
    func downloadMediaDidTap(_ button: UIButton)
    func callToPlaceOrderDidTap(_ button: UIButton)
}

class ProductDetailView: UIView {
	
    var delegate: ProductDetailViewDelegate?
    
	let collectionView: UICollectionView = {
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
		collectionView.backgroundColor = .init(white: 0.95, alpha: 1)
		collectionView.alwaysBounceVertical = true
		return collectionView
	}()
	
	let toolbar: UIView = {
		let view = UIView()
		view.backgroundColor = .white
		return view
	}()
	
    lazy var callBtn = setupActionBtn(title: "Appelez pour commander",
                                               titleColor: .white,
                                               backgroundColor: #colorLiteral(red: 0.9803921569, green: 0.2745098039, blue: 0.2745098039, alpha: 1),
                                               action: #selector(callDidTap))
    
    lazy var downloadMediaBtn = setupActionBtn(title: "Sauvegarder Video/Images",
                                                titleColor: #colorLiteral(red: 0.9803921569, green: 0.2745098039, blue: 0.2745098039, alpha: 1),
                                                backgroundColor: #colorLiteral(red: 0.9715830684, green: 0.9879427552, blue: 0.9154971242, alpha: 1),
                                                action: #selector(downloadMediaDidTap))
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupViews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupViews() {
		backgroundColor = .init(white: 0.95, alpha: 1)
		
		// Setup Toolbar
		let btnStackView = UIStackView(arrangedSubviews: [downloadMediaBtn, callBtn])
		btnStackView.distribution = .fillProportionally
		toolbar.addSubview(btnStackView)
		
		btnStackView.trailing(to: toolbar)
			.leading(to: toolbar)
			.top(to: toolbar)
			.bottom(to: toolbar)
		
		// Setup ProductDetaiView
		add(subviews: [collectionView, toolbar])
		collectionView.top(to: self)
			.leading(to: self)
			.trailing(to: self)
		
		toolbar.top(with: collectionView.bottomAnchor)
			.leading(to: self)
			.trailing(to: self)
			.bottom(with: layoutMarginsGuide.bottomAnchor)
			.height(value: 49)
	}
	
    private func setupActionBtn(title: String, titleColor: UIColor, backgroundColor: UIColor, action: Selector) -> UIButton {
		let btn = UIButton(type: .system)
		btn.setTitle(title, for: .normal)
		btn.titleLabel?.font = Theme.Fonts.proximaNovaBold(size: 14).value
		btn.setTitleColor(titleColor, for: .normal)
		btn.backgroundColor = backgroundColor
        btn.addTarget(self, action: action, for: .touchUpInside)
		return btn
	}
    
    @objc func downloadMediaDidTap() {
        delegate?.downloadMediaDidTap(downloadMediaBtn)
    }
    
    @objc func callDidTap() {
        delegate?.callToPlaceOrderDidTap(callBtn)
    }
}
