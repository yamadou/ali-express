//
//  ProductDetailViewModel.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 2/1/21.
//

import UIKit

enum ProductDetailSection: Int {
	case description = 0
	case variant = 1
}

class ProductDetailViewModel {
	
    var product: Product
    
    init(product: Product) {
        self.product = product
    }
    
	func getSectionType(_ section: Int) -> ProductDetailSection {
		return ProductDetailSection.init(rawValue: section)!
	}
    
    func makePhoneCall() {
        guard let numberUrl = URL(string: "tel://778292807") else { return }
        UIApplication.shared.open(numberUrl)
    }
    
    func saveImagesToSavedPhotosAlbum(completion: @escaping () -> ()) {
        let group = DispatchGroup()
        for url in product.imagesUrl {
            group.enter()
            Service.shared.getMediaData(forUrl: url) { data in
                guard let data = data, let image = UIImage(data: data) else { return }
                PhotosAppMediaSaver.shared.saveImageToPhotoAlbum(image: image)
                group.leave()
            }
        }
        group.notify(queue: .global()) {
            completion()
        }
    }
    func saveVideoToSavedPhotosAlbum(urlString: String, completion: @escaping () -> ()) {
        getVideoData(urlString: urlString) { data in
            guard let videoData = data else { return }
            PhotosAppMediaSaver.shared.saveVideoToPhotoAlbum(data: videoData) {
                completion()
            }
        }
    }
    
    private func getVideoData(urlString: String, completion: @escaping (Data?) -> ()) {
        guard let videoUrlString = product.videosUrl.first else { return }
        
        // retrieve data from memory
        if let videoData = Service.shared.getVideoDataFromMemory(videoUrl: videoUrlString ?? "") {
            completion(videoData)
            return
        }
        
        // retrieve data from remote url
        Service.shared.getDownloadUrl(for: urlString) { (url, err) in
            if let _ = err { return }
            guard let downloadUrl = url else { return }
            Service.shared.getMediaData(forUrl: downloadUrl.absoluteString) { data in
                completion(data)
                guard let data = data else { return }
                Service.shared.saveVideoDataToMemory(videoUrl: videoUrlString ?? "", data: data)
            }
        }
    }
    
}


