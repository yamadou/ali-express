//
//  Product.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 1/30/21.
//

import Foundation
import FirebaseFirestoreSwift

struct Product: Identifiable, Codable {
	@DocumentID var id: String?
	let name: String
	let price: Int
	let MOQ: Int
    let recommendedRetailPrice: Int
	let description: String
	let imagesUrl: [String]
    let videosUrl: [String?]
	
	enum CodingKeys: String, CodingKey {
		case name
		case price = "unitPrice"
        case recommendedRetailPrice
		case MOQ = "minimumOrder"
		case description
		case imagesUrl = "images"
        case videosUrl = "videos"
	}
}
