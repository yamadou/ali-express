//
//  Service.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 1/30/21.
//

import Foundation
import Firebase

class Service {
	
	static let shared = Service() // Singleton
    
    var imageDataCache = NSCache<NSString, NSData>()
    var videoDataCache = NSCache<NSString, NSData>()
    var videoDonwloadUrlCache = NSCache<NSString, NSURL>()
    
    private init() {}
	
	let db = Firestore.firestore()
    
	func fetchProducts(completion: @escaping([Product]?, Error?) -> ()) {
		db.collection("products").getDocuments() { (querySnapshot, err) in
			if let err = err {
				print("Error getting documents: \(err)")
				completion(nil, err)
			} else {
				guard let documents = querySnapshot?.documents else { return }
                print("documents: \(documents.description)")
				let products = documents.compactMap { queryDocumentSnapshot -> Product? in
					return try? queryDocumentSnapshot.data(as: Product.self)
				}
				completion(products, nil)
			}
		}
	}
    
    func getMediaData(forUrl: String?, completion: @escaping (Data?) -> ()) {
        guard let mediaUrl = forUrl else { return }
        if let cachedImageData = imageDataCache.object(forKey: mediaUrl as NSString) {
            completion(cachedImageData as Data)
            return
        }
        
        let storage = Storage.storage()
        let gsReference = storage.reference(forURL: mediaUrl)
        gsReference.getData(maxSize: 20 * 1024 * 1024) { (data, err) in
            guard err == nil else  {
                print("error downloading data: \(String(describing: err?.localizedDescription))")
                return
            }
            guard let data = data else { completion(nil); return }
            self.imageDataCache.setObject(data as NSData, forKey: mediaUrl as NSString)
            completion(data)
        }
    }
    
    func getDownloadUrl(for url: String?, completion: @escaping (URL?, Error?) -> ()) {
        guard let mediaUrl = url else { return }
        if let cachedDonwloadUrl = videoDonwloadUrlCache.object(forKey: mediaUrl as NSString) {
            completion(cachedDonwloadUrl as URL, nil)
            return
        }
        let storage = Storage.storage()
        let gsReference = storage.reference(forURL: mediaUrl)
        gsReference.downloadURL { (url, err) in
            if let url = url {
                self.videoDonwloadUrlCache.setObject(url as NSURL, forKey: mediaUrl as NSString)
            }
            completion(url, err)
        }
    }
    
    func saveVideoDataToMemory(videoUrl: String, data: Data) {
        videoDataCache.setObject(data as NSData, forKey: videoUrl as NSString)
    }
    
    func getVideoDataFromMemory(videoUrl: String) -> Data? {
        return videoDataCache.object(forKey: videoUrl as NSString) as Data?
    }
}
