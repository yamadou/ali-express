//
//  MediaSaver.swift
//  Afridis_iOS
//
//  Created by Yamadou Traore on 2/16/21.
//

import UIKit
import Photos

class PhotosAppMediaSaver: NSObject {
    
    static let shared = PhotosAppMediaSaver() // Singleton
    
    private override init() {}
    
    func saveImageToPhotoAlbum(image: UIImage) {
        requestAuthorization {
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.saveError), nil)
        }
    }
    
    @objc func saveError(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        guard error == nil else {
            print("error saving image: \(error?.localizedDescription ?? "")")
            return
        }
        print("did Finish Saving saving Image: \(image)")
    }
    
    func requestAuthorization(completion: @escaping ()->Void) {
        if PHPhotoLibrary.authorizationStatus() == .notDetermined {
            PHPhotoLibrary.requestAuthorization { (status) in
                DispatchQueue.main.async {
                    completion()
                }
            }
        } else if PHPhotoLibrary.authorizationStatus() == .authorized{
            completion()
        }
    }

    func saveVideoToPhotoAlbum(data: Data, completion: @escaping () -> ()) {
        requestAuthorization {
            DispatchQueue.global(qos: .background).async {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                let filePath="\(documentsPath)/tempFile.mp4"
                DispatchQueue.main.async {
                    (data as NSData).write(toFile: filePath, atomically: true)
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                    }) { completed, error in
                        guard error == nil else {
                            print("Error Saving Video to Photos: \(error?.localizedDescription ?? "")")
                            completion()
                            return
                        }
                        if completed {
                            try? FileManager.default.removeItem(atPath: filePath)
                            print("Video is saved!")
                        }
                        completion()
                    }
                }
            }
        }
    }
}
